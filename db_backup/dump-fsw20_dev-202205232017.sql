--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

-- Started on 2022-05-23 20:17:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE fsw20_dev;
--
-- TOC entry 3330 (class 1262 OID 24763)
-- Name: fsw20_dev; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE fsw20_dev WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_Indonesia.1252';


ALTER DATABASE fsw20_dev OWNER TO postgres;

\connect fsw20_dev

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 211 (class 1259 OID 24790)
-- Name: Articles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Articles" (
    id bigint NOT NULL,
    tipe_data text,
    dataset json,
    dataset_jsonb jsonb,
    "user" text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    data_status text
);


ALTER TABLE public."Articles" OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 24789)
-- Name: Articles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Articles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Articles_id_seq" OWNER TO postgres;

--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 210
-- Name: Articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Articles_id_seq" OWNED BY public."Articles".id;


--
-- TOC entry 209 (class 1259 OID 24784)
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 24803)
-- Name: newview; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.newview AS
 SELECT (b.dataset_jsonb ->> 'id'::text) AS datasetid,
    (a.dataset_jsonb ->> 'user_id'::text) AS userid,
    (a.dataset_jsonb ->> 'game_played'::text) AS game,
    (a.dataset_jsonb ->> 'player_score'::text) AS playerscore,
    (a.dataset_jsonb ->> 'comp_score'::text) AS compscore,
    (a.dataset_jsonb ->> 'result'::text) AS result,
    a."createdAt" AS dateplayed
   FROM (public."Articles" a
     JOIN public."Articles" b ON (((a.dataset_jsonb ->> 'id'::text) = (b.dataset_jsonb ->> 'id'::text))))
  WHERE (((b.dataset_jsonb ->> 'group'::text) = 'player'::text) AND (a.tipe_data = 'UserGameHistory'::text));


ALTER TABLE public.newview OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 24809)
-- Name: view_user_data; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_user_data AS
 SELECT "Articles".id AS datasetid,
    ("Articles".dataset_jsonb ->> 'user_id'::text) AS userid,
    ("Articles".dataset_jsonb ->> 'email'::text) AS email,
    ("Articles".dataset_jsonb ->> 'first_name'::text) AS firstname,
    ("Articles".dataset_jsonb ->> 'last_name'::text) AS lastname
   FROM public."Articles"
  WHERE ((("Articles".dataset_jsonb ->> 'group'::text) = 'player'::text) AND ("Articles".data_status = 'Active'::text));


ALTER TABLE public.view_user_data OWNER TO postgres;

--
-- TOC entry 3176 (class 2604 OID 24793)
-- Name: Articles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Articles" ALTER COLUMN id SET DEFAULT nextval('public."Articles_id_seq"'::regclass);


--
-- TOC entry 3324 (class 0 OID 24790)
-- Dependencies: 211
-- Data for Name: Articles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Articles" VALUES (8, 'UserGameHistory', '{
	"id":4,
	"user_id": "ryan",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 4,
	"comp_score": "1",
	"result": "WIN"

}', '{"id": 4, "result": "WIN", "user_id": "ryan", "comp_score": "1", "game_played": "ROCK PAPER SCISSORS", "player_score": 4}', 'admin', '2022-05-22 16:46:12.240165+08', '2022-05-22 16:46:12.240165+08', 'Active');
INSERT INTO public."Articles" VALUES (5, 'UserLogin', '{
	"user_id": "ryan",
	"ip": "10.29.01.93"
}', '{"ip": "10.29.01.93", "user_id": "ryan"}', 'admin', '2022-05-22 16:39:04.171418+08', '2022-05-22 16:39:04.171418+08', 'Active');
INSERT INTO public."Articles" VALUES (12, 'UserLogin', '{
	"user_id": "ryan",
	"ip": "10.29.01.93"
}', '{"ip": "10.29.01.93", "user_id": "ryan"}', 'admin', '2022-05-22 16:47:05.522859+08', '2022-05-22 16:47:05.522859+08', 'Active');
INSERT INTO public."Articles" VALUES (15, 'UserLogin', '{
	"user_id": "ryan",
	"ip": "10.29.01.101"
}', '{"ip": "10.29.01.101", "user_id": "ryan"}', 'admin', '2022-05-22 16:48:10.260304+08', '2022-05-22 16:48:10.260304+08', 'Active');
INSERT INTO public."Articles" VALUES (9, 'UserGameHistory', '{
	"id":4,
	"user_id": "ryan",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 3,
	"comp_score": "2",
	"result": "WIN"

}', '{"id": 4, "result": "WIN", "user_id": "ryan", "comp_score": "2", "game_played": "ROCK PAPER SCISSORS", "player_score": 3}', 'admin', '2022-05-22 16:46:19.285427+08', '2022-05-22 16:46:19.285427+08', 'Active');
INSERT INTO public."Articles" VALUES (16, 'UserGameHistory', '{
	"id":2,
	"user_id": "webb",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 4,
	"comp_score": "1",
	"result": "WIN"

}', '{"id": 4, "result": "WIN", "user_id": "webb", "comp_score": 1, "game_played": "ROCK PAPER SCISSORS", "player_score": 4}', 'admin', '2022-05-22 22:15:52.496595+08', '2022-05-22 22:15:52.496595+08', 'Active');
INSERT INTO public."Articles" VALUES (10, 'UserGameHistory', '{
	"id":4,
	"user_id": "ryan",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 5,
	"comp_score": "0",
	"result": "WIN"

}', '{"id": 4, "result": "WIN", "user_id": "ryan", "comp_score": "0", "game_played": "ROCK PAPER SCISSORS", "player_score": 5}', 'admin', '2022-05-22 16:46:29.775306+08', '2022-05-22 16:46:29.775306+08', 'Active');
INSERT INTO public."Articles" VALUES (11, 'UserGameHistory', '{
	"id":4,
	"user_id": "ryan",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 2,
	"comp_score": "3",
	"result": "LOSE"

}', '{"id": 4, "result": "LOSE", "user_id": "ryan", "comp_score": "3", "game_played": "ROCK PAPER SCISSORS", "player_score": 2}', 'admin', '2022-05-22 16:46:48.305047+08', '2022-05-22 16:46:48.305047+08', 'Active');
INSERT INTO public."Articles" VALUES (1, 'ConfigUser', '{
"id":1,
	"user_id": "one",
	"password": "one",
	"email":"one@gmail.com",
	"first_name": "Irwan",
	"last_name": "Adinatha",
	"group": "admin"
}', '{"id": 1, "email": "one@gmail.com", "group": "admin", "user_id": "one", "password": "one", "last_name": "Adinatha", "first_name": "Irwan"}', 'admin', '2022-05-22 16:28:36.76551+08', '2022-05-22 16:28:36.76551+08', 'Active');
INSERT INTO public."Articles" VALUES (2, 'ConfigUser', '{
"id":2,
	"user_id": "webb",
	"password": "webb",
	"email":"webb@gmail.com",
	"first_name": "David",
	"last_name": "Webb",
	"group": "player"
}', '{"id": 2, "email": "webb@gmail.com", "group": "player", "user_id": "webb", "password": "webb", "last_name": "Webb", "first_name": "David"}', 'admin', '2022-05-22 16:30:58.080102+08', '2022-05-22 16:30:58.080102+08', 'Active');
INSERT INTO public."Articles" VALUES (18, 'UserGameHistory', '{
	"id":2,
	"user_id": "webb",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 1,
	"comp_score": "4",
	"result": "LOSE"

}', '{"id": 2, "result": "LOSE", "user_id": "webb", "comp_score": 4, "game_played": "ROCK PAPER SCISSORS", "player_score": 1}', 'admin', '2022-05-22 22:16:29.422251+08', '2022-05-22 22:16:29.422251+08', 'Active');
INSERT INTO public."Articles" VALUES (20, 'ConfigUser', '{"id":0,"user_id":"croft","password":"croft","email":"croft@gmail.com","first_name":"Lara","last_name":"Croft","group":"player"}', '{"id": 0, "email": "croft@gmail.com", "group": "player", "user_id": "croft", "password": "croft", "last_name": "Croft", "first_name": "Lara"}', 'admin', '2022-05-23 16:14:20.645+08', '2022-05-23 16:14:20.645+08', 'Active');
INSERT INTO public."Articles" VALUES (21, 'ConfigUser', '{"id":0,"user_id":"damir","password":"damir","email":"damir@gmail","first_name":"Damir","last_name":"Handoyo","group":"player"}', '{"id": 0, "email": "damir@gmail", "group": "player", "user_id": "damir", "password": "damir", "last_name": "Handoyo", "first_name": "Damir"}', 'admin', '2022-05-23 19:53:42.424+08', '2022-05-23 19:53:42.424+08', 'Active');
INSERT INTO public."Articles" VALUES (22, 'ConfigUser', '{"id":0,"user_id":"ivy","password":"ivy","email":"ivy@gmail.com","first_name":"Ivy","last_name":"Katuari","group":"player"}', '{"id": 0, "email": "ivy@gmail.com", "group": "player", "user_id": "ivy", "password": "ivy", "last_name": "Katuari", "first_name": "Ivy"}', 'admin', '2022-05-23 19:54:53.871+08', '2022-05-23 19:54:53.871+08', 'Active');
INSERT INTO public."Articles" VALUES (19, 'ConfigUser', '{"id":0,"password":"depp","email":"depp@gmail.com","group":"player"}', '{"id": 0, "email": "depp@gmail.com", "group": "player", "password": "depp"}', 'admin', '2022-05-23 16:13:21.329+08', '2022-05-23 16:13:21.329+08', 'VOID');
INSERT INTO public."Articles" VALUES (4, 'ConfigUser', '{
	"id":4,
	"user_id": "ryan",
	"password": "ryan",
	"email":"jeriryan@gmail.com",
	"first_name": "Jeri",
	"last_name": "Ryan",
	"group": "player"
}', '{"id": 4, "email": "jeriryan@gmail.com", "group": "player", "user_id": "ryan", "password": "ryan", "last_name": "Ryan", "first_name": "Jeri"}', 'admin', '2022-05-22 16:36:11.580267+08', '2022-05-22 16:36:11.580267+08', 'Active');
INSERT INTO public."Articles" VALUES (13, 'UserGameHistory', '{
	"id":4,
	"user_id": "ryan",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 4,
	"comp_score": "1",
	"result": "WIN"

}', '{"id": 4, "result": "WIN", "user_id": "ryan", "comp_score": "1", "game_played": "ROCK PAPER SCISSORS", "player_score": 4}', 'admin', '2022-05-22 16:47:48.408095+08', '2022-05-22 16:47:48.408095+08', 'Active');
INSERT INTO public."Articles" VALUES (14, 'UserGameHistory', '{
	"id":4,
	"user_id": "ryan",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 3,
	"comp_score": "2",
	"result": "WIN"

}', '{"id": 4, "result": "WIN", "user_id": "ryan", "comp_score": "2", "game_played": "ROCK PAPER SCISSORS", "player_score": 3}', 'admin', '2022-05-22 16:47:56.056532+08', '2022-05-22 16:47:56.056532+08', 'Active');
INSERT INTO public."Articles" VALUES (17, 'UserGameHistory', '{
	"id":2,
	"user_id": "webb",
	"game_played": "ROCK PAPER SCISSORS",
	"player_score": 3,
	"comp_score": "2",
	"result": "WIN"

}', '{"id": 2, "result": "WIN", "user_id": "webb", "comp_score": 2, "game_played": "ROCK PAPER SCISSORS", "player_score": 3}', 'admin', '2022-05-22 22:16:16.740343+08', '2022-05-22 22:16:16.740343+08', 'Active');
INSERT INTO public."Articles" VALUES (23, 'ConfigUser', '{"id":0,"user_id":"depp","password":"depp","email":"depp@yahoo.com","first_name":"Johnny","last_name":"Depp","group":"player"}', '{"id": 0, "email": "depp@yahoo.com", "group": "player", "user_id": "depp", "password": "depp", "last_name": "Depp", "first_name": "Johnny"}', 'admin', '2022-05-23 20:00:49.607+08', '2022-05-23 20:00:49.607+08', 'Active');


--
-- TOC entry 3322 (class 0 OID 24784)
-- Dependencies: 209
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."SequelizeMeta" VALUES ('20220522065847-create-article.js');


--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 210
-- Name: Articles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Articles_id_seq"', 23, true);


--
-- TOC entry 3180 (class 2606 OID 24797)
-- Name: Articles Articles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Articles"
    ADD CONSTRAINT "Articles_pkey" PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 24788)
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


-- Completed on 2022-05-23 20:17:04

--
-- PostgreSQL database dump complete
--

