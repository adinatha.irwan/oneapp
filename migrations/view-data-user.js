const view_name = 'view_user_data';
const query = '';


module.exports = {
  up: function (database, Sequelize) {
    return database.query(`CREATE VIEW ${view_name} AS ${query}`);
  },
  down: function (database, Sequelize) {
    return database.query(`DROP VIEW ${view_name}`);
  }
}