const PORT = process.env.PORT || 3000;
const express = require("express");
const app = express();
const path = require('path');
const bodyParser = require("body-parser");

const fs = require('fs');
const fsPromises = require('fs').promises;
const logEvents = require ('./logEvents');
const appAPI =  require('./controllers/API'); // import the api controllers
const EventEmitter = require('events');
class Emitter extends EventEmitter {};
//init object
const myEmitter = new Emitter();

const loginRouter = require("./routes/login.js");
const dashboardRouter = require("./routes/dashboard.js");
const apidashboardRouter = require("./routes/apidashboard.js");

let linkLoginText = "LOGIN";
let linkSignupText = "SIGN UP";

app.use(bodyParser.urlencoded({extended:true}));

//use the API router s
const router  = require('./controllers/API');
const exp = require("constants");
app.use('/', router);


// use usermanagement router
const userMgmt  = require('./controllers/UserManagement/userMgmt.js');
app.use('/', userMgmt);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname,'views', 'Chapter3_Wave20')));
app.use(express.static(path.join(__dirname,'views', 'challenge_4')));
app.use(express.static(path.join(__dirname,'views', '404')));
app.use(express.static(path.join(__dirname,'views')));
app.use("/login", loginRouter);
app.use("/dashboard",dashboardRouter);
app.use("/apidashboard",apidashboardRouter);


app.get('^/$|/index(.html)?', (req, res) => {
    
    res.render(path.join(__dirname,'views', 'Chapter3_Wave20', 'index.ejs'),{linkSignupText, linkLoginText});

})

// app.get('/login', (req, res) => {
//     console.log("got here to get / of the login view");
// //    res.render("login", { title: "Login Page" })
//    res.render(path.join(__dirname,'views',  'login.ejs'), { title: "Login Page" });
// })

app.post('/', (req, res) => {
    res.render(path.join(__dirname,'views', 'Chapter3_Wave20', 'index.ejs'),{linkSignupText, linkLoginText});

})

app.post('/rockpaperstrategy', (req, res) => {
    res.sendFile(path.join(__dirname,'views', 'challenge_4', 'rockpapergame.html'));
})

// app.post('^/$|/login(.html)?', (req, res) => {
//     const email  = req.body.inputEmail ;
//     console.log(email);
//     // res.sendFile(path.join(__dirname,'views', 'Chapter3_Wave20', 'index.html'));
// })




//handle 404
app.get('/*', (req, res) => {

    res.status(404).sendFile(path.join(__dirname,'views', '404', '404.html'));
})















app.listen(PORT, () => console.log(`Server running on PORT ${PORT}`));



// // add listener for the log event
// myEmitter.on('log', (msg) => logEvents(msg));

//     myEmitter.emit('log', 'Log Event Emitted');
