const express = require("express");
const Sequelize = require("sequelize");
const { QueryTypes } = require('sequelize');
const bodyParser = require("body-parser");
const router = express();
const { Article } = require('../models');
const { json } = require("express/lib/response");
router.use(express.Router());
router.use(express.json());
router.use(bodyParser.urlencoded({extended:false}));
const https = require("https");

// routes API
//getting all
router.get('/', async (req, res) =>{
    Article.findAll({
        where: {
            data_status :'Active',
            tipe_data: 'ConfigUser',
            "dataset_jsonb.group": 'player'
        }

      }).then(datamessage => { 
      res.status(201).json(datamessage)})
})



//getting  one not working yet
router.get('/:userid', (req, res) =>{
    const userId= req.params.userid
    Article.findOne({
        where: {
            "dataset_jsonb.user_id": userId,
            data_status :'Active',
            tipe_data: 'ConfigUser',
            "dataset_jsonb.group": 'player'
        }
    }).then(datamessage => { 
      res.status(201).json(datamessage)})
})
//creating one
router.post('/', async (req, res) =>{
    //check if username alreadtaken
    const userId= req.body.username;
    console.log(userId)
    Article.findAll({
        where: {
            "dataset_jsonb.user_id": userId,
            data_status :'Active',
            tipe_data: 'ConfigUser',
            "dataset_jsonb.group": 'player'
        }
    }).then(datamessage => { 
        if(datamessage.length  > 0 ){
            console.log(">>datamessage>>")
            console.log(datamessage)
            console.log("user exist")
            res.status(500).json({ message:"User Name already Exists" });
        } else {
            console.log("user does not exist")
            //if not taken then insert
            const subscribers = new Article({
                //data struct here
                tipe_data: 'ConfigUser',
                dataset: {
                    id:0,
                    user_id: req.body.username,
                    password: req.body.password,
                    email: req.body.email,
                    first_name: req.body.firstname,
                    last_name: req.body.lastname,
                    group: 'player'
                },
                dataset_jsonb: {
                    id:0,
                    user_id: req.body.username,
                    password: req.body.password,
                    email: req.body.email,
                    first_name: req.body.firstname,
                    last_name: req.body.lastname,
                    group: 'player'
                },
                user: 'admin',
                data_status: 'Active'
                
            })
            try{
                const newSubscribers = subscribers.save();
                // need to update id in dataset here
                res.status(201).json(subscribers);

            } catch (err){
                res.status(400).json({ message:err.message });
            }

        }
        
      //res.status(201).json(datamessage)
    })
    



    
})
//updating one
router.patch('/', (req, res) =>{
    
})
// deleting one
router.delete('/:id', (req, res) =>{
    
})


module.exports = router;